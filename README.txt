/*----------------------------------
*			 	 Projet 
*		D�veloppement en environnement 
*			de base de donn�e
*
*			   	   TP3
*
*			 	�quipe
*			NAZER LEE-ST�NIO
*			SARAH MEI L�ONARD
*			IPRAHIM TUTUNCU
*
*
------------------------------------*/

***********************************************************************************************

	MODE BASE DE DONN�E
---------------------------------------------------
Utiliser fichier TP2.py
	est li� avec:
		Config.py
		ConnexionCxOracle
		cluster.py
		synonym.py
		entrainement.py
		ecrireFichier.py
		


FAIRE UN ENTRAINEMENT
--------------------------
python TP2.py -e -t (fenetre) -enc (encodage) -cc (lien vers fichier)


FAIRE UNE RECHERCHE DE SYNONYMES
--------------------------------------
python TP2.py -s -t (fenetre)


FAIRE UN CLUSTERING
-------------------------
python TP2.py -nc (nombre de clusters) -n (nombre de mots � afficher) -t (fenetre)
	Pour utiliser une liste de mots, ajouter le param�tre -m 
		Mettre la liste de mots entre guillements et s�parer d'un espace
		
	Si aucune liste de mot n'est sp�cifi�, cr�e cluster avec positionnement orthogonal
		
*************************************************************************************************
		
	MODE FICHIER CSV
-------------------------------------------------------------
Utiliser fichier testCSV.py
	est li� avec:
		clusterCSV.py
		synonym.py
		entrainement.py
		ecrireFichier.py
		CSVData.py

IMPORTANT : mot.csv et concurrence.csv doivent exister dans le m�me dossier que testCSV.py

FAIRE UN ENTRAINEMENT
--------------------------
python testCSV.py -e -t (fenetre) -enc (encodage) -cc (lien vers fichier)


FAIRE UNE RECHERCHE DE SYNONYMES
--------------------------------------
python testCSV.py -s -t (fenetre)


FAIRE UN CLUSTERING
-------------------------
python testCSV.py -nc (nombre de clusters) -n (nombre de mots � afficher) -t (fenetre)
	Pour utiliser une liste de mots, ajouter le param�tre -m 
		Mettre la liste de mots entre guillements et s�parer d'un espace
		
	Si aucune liste de mot n'est sp�cifi�, cr�e cluster avec random cluster

